# Add host.docker.internal

```host.docker.internal``` points to host machine's 0.0.0.0

```docker-entrypoint``` finds IP of host machine in docker container and adds it with the DNS to ```/etc/hosts```.

```CMD ["sh", "run.sh"]``` in ```Dockerfile``` must have ```sh``` in order not to suffer permission issue when running shell scripts.

**Note:** must ```chmod +x``` all the shell scripts and enable bash in docker container.

### Test

Go to docker container using command: ```docker exec -it $container_name bash``` and replace ```$container_name``` with your container name.

Curl the host machine's running service in port ```$PORT``` using: ```curl http://host.docker.internal:$PORT```.